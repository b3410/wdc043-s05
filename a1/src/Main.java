public class Main {
    public static void main(String[] args) {
        User user1 = new User("Erika","Javier", 23, "Laguna");
        System.out.println("User's First Name:");
        System.out.println(user1.getFirstName());
        System.out.println("User's Last Name:");
        System.out.println(user1.getLastName());
        System.out.println("User's Age:");
        System.out.println(user1.getAge());
        System.out.println("User's Address:");
        System.out.println(user1.getAddress());

        Course course1 = new Course("Algebra", "learn x", 30, 25.23, "May 2022", "June 2022", user1);
        System.out.println("Course's Name:");
        System.out.println(course1.getName());
        System.out.println("Course's Description:");
        System.out.println(course1.getDescription());
        System.out.println("Course's seats:");
        System.out.println(course1.getSeats());
        System.out.println("Course's fee:");
        System.out.println(course1.getFee());
        System.out.println("Course's Instructor's First Name:");
        System.out.println(course1.getUserStudent().getFirstName());
        System.out.println("Course's Instructor's Last Name:");
        System.out.println(course1.getUserStudent().getLastName());
    }
}